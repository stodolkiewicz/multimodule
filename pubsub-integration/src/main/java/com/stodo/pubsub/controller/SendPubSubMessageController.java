package com.stodo.pubsub.controller;

import com.stodo.pubsub.configuration.PubSubSenderConfiguration;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class SendPubSubMessageController {
    PubSubSenderConfiguration.PubsubOutboundGateway pubsubOutboundGateway;

    public SendPubSubMessageController(PubSubSenderConfiguration.PubsubOutboundGateway pubsubOutboundGateway) {
        this.pubsubOutboundGateway = pubsubOutboundGateway;
    }

    @PostMapping
    public ResponseEntity<String> sendMessage (@RequestParam String message) {
        pubsubOutboundGateway.sendToPubsub(message);
        return new ResponseEntity<>(message, HttpStatus.OK);
    }

}
